## Challenge Rick and Morty API

Este proyecto consiste en poder visualizar la lista de episodios de la serie de Rick and Morty, y que te muestre al hacer click en cada episodio un detalle del mismo.

## Liberias usadas

`react-modal; dayjs; react v17, axios`

## Para estilar la web

Se uso css puro.

## Conclusion

Hacer el proyecto estuvo divertido, lo hice con ts porque me parece mucho mas limpio y te ayuda a saber el tipo de dato que se le envia a cada componente o funcion.
Para poder correr el programa solo se necesita escribir el siguiente comando.

### `npm start`
