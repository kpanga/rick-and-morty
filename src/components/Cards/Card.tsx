import './Card.css';

import imgSrc from '../../assets/icon-RickAndMorty-folks.svg';
import { Episode } from '../../models/episode.model';

import dayjs from "dayjs";
import 'dayjs/locale/es'
import Modal from 'react-modal';
import { useState } from 'react';
import DetailsEpisode from '../DetailEpisode/DetailsEpisode';
import { useMediaQuery } from 'react-responsive';


interface Props {
    episode: Episode;
}

let isTabletOrMobile = false;

const Card = ({episode}:Props) => {
  isTabletOrMobile = useMediaQuery({ query: '(max-width: 900px)' });
  const [ isOpen, setIsOpen ] = useState(false);

  const formatDate = (fecha:string) => {
    return fecha.charAt(0).toUpperCase() + fecha.slice(1);
  }
  const detailModal = () => {
    return (
      <Modal
        isOpen={isOpen}
        onRequestClose={() => setIsOpen(false)}
        style={{
          content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            borderRadius: '25px',
            width: isTabletOrMobile ? '80%' : '30%',
            padding: isTabletOrMobile ? '5px' : '20px'
          },
          overlay: {
            backgroundColor: 'rgba(255, 255, 255, 0.5)'
          }
        }}
      >
       <DetailsEpisode 
       episode={episode} 
       closed={ () => setIsOpen(false)}
       />
      </Modal>
    )
  }

  return (
    <div className='card'>
      <div className='card-image--wrapper'>
        <img className='card-image' src={imgSrc} alt={'Rick and Morty'} />
      </div>
      <div className='card-body'>
        <h2  className='card-title' style={{ display: 'flex', flexDirection: 'row' }}>
          <span>#{episode.id}</span> {episode.name} - {episode.episode}
        </h2>
        <h3 style={{ display: 'flex', flexDirection: 'column'}}>
          Emitido en la fecha: 
          {/* {dayjs(episode.air_date).format('DD/MM/YYYY')} */}
          <p>{formatDate(dayjs(episode.air_date).locale('es').format('dddd'))} {dayjs(episode.air_date).format('DD')} de {formatDate(dayjs(episode.air_date).locale('es').format('MMMM'))} del {dayjs(episode.air_date).format('YYYY')}</p>
        </h3>
        <div>
          <button type="button" className="btn-detail" onClick={() => setIsOpen(true)}>Ver detalle</button>
        </div>
      </div>
      {detailModal()}
    </div>
  );
};

export default Card;
