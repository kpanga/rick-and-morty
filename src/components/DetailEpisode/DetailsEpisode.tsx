import dayjs from "dayjs"
import { useEffect, useState } from "react"
import { Character } from "../../models/character.model"
import { Episode } from "../../models/episode.model"

import './DetailsEpisode.css'

interface Props {
    episode: Episode;
    closed?: any;
    
}
let auxCharacters: Character[] = [];
const DetailsEpisode = ({ episode, closed }: Props) => {
    const [ characters, setCharacters ] = useState<Character[]>([]);

    useEffect(() => {
        const getNextCharacters = async () => {
            for (let index = 0; index < 7; index++) {
                const response = await fetch(episode.characters[index]);
                const data = await response.json()
                auxCharacters.push(data);
            }
            setCharacters(auxCharacters);
        }
        getNextCharacters()
    }, []);

    const resetCharacters =  () => {
        auxCharacters = [];
        setCharacters(auxCharacters);
        closed();
    }

    return (
        <div onClick={resetCharacters} className="close-modal">
            <div style={{display: 'flex', justifyContent:'flex-end'}}>
                <button onClick= {resetCharacters} >X</button>
            </div>
            <h1>
                <span style={{ color: 'black' }}>#{episode.id}</span> Titulo: {episode.name}
            </h1>
            <p>Fecha de Emision: {dayjs(episode.air_date).format('DD-MM-YYYY')}</p>
            <p>Temporada: {episode.episode.substring(0, 3)}</p>
            <p>Numero de Personajes por episodio: {episode.characters.length}</p>
            <p>Algunos personajes:</p>
           <div style={{display: 'flex', flexDirection: 'row', flexWrap:'wrap'}}>
                {
                    characters.map((character: Character, index: number) => {
                        return (
                            <div key={character.id}>
                                {
                                    index === characters.length -1 ?
                                    <p>{character.name}</p>
                                    :
                                    <p>{character.name},</p>
                                }
                            </div>
                        )
                    })
                }
           </div>
        </div>
    )
}

export default DetailsEpisode
