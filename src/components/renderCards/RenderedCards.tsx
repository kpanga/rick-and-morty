import { useEffect, useState } from 'react';
// Internal Stylesheets
import './RenderedCards.css';
// Assets
import icon from '../../assets/icon-RickAndMorty-glow.svg';

import Card from '../Cards/Card';
import { Episode } from '../../models/episode.model';

interface Props {
  results: any;
}
const RenderedCards = ({ results }: Props) => {

  const count = results?.info?.count;
  const [next, setNext] = useState<string>(results?.info?.next);
  const [items, setItems] = useState<Episode[]>(results?.results);
  const [loading, setLoading] = useState(false);
  const [noData, setNoData] = useState(false);

  window.onscroll = () => {
    if (window.innerHeight + document.documentElement.scrollTop  <= document.documentElement.offsetHeight) {
      if (next) {
        getNextCharacters(next);
      } else {
        setNoData(true);
      }
    }
  };

  const getNextCharacters = (endpoint: string) => {
    fetch(endpoint)
      .then(response => response.json())
      .then(data => {
        console.log('Success:', data);
        const newItems = [...items, ...data.results];
        setItems([...newItems]);
        // const newItems = items.concat(data.results);
        // setItems(newItems);
        setNext(data.info.next);
        if (data.length === 0) setNoData(true);
        if (data.results.length < 20) setNoData(true);
      })
      .catch(error => {
        console.error('Error:', error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <section className='rendered-cards'>
      {items?.length > 0 && <div>
        <div className='rendered-cards--header'>
          <h3 style={{color:'black'}}>Numero de Episodios: {count}</h3>
        </div>
        <div className='rendered-cards--results'>
          {items.map((episode: Episode, index: number) => {
            return (
              <Card
                key={episode.id}
                episode={episode}
              />
            );
          })}
        </div>
      </div>}
      <div className='search-status'>
        {loading ? <h3 className='search-status--message'>loading data ...</h3> : ''}
        {noData || items?.length < 20 ? <img
          className='search-status--icon'
          src={icon}
          alt='rick and morty no more results'
        /> : ''}
      </div>
    </section>
  );
};

export default RenderedCards;