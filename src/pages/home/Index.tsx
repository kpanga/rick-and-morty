// External imports
import { useState, useEffect } from 'react';
import axios from 'axios';

// Internal Stylesheets
import './Home.css';

// Assets
import logo from '../../assets/logo-RickAndMorty.svg';
import characteropedia from '../../assets/Characteropedia.svg';
import icon from '../../assets/icon-RickAndMorty-NoSearchResults.svg';

// Components
import RenderedCards from '../../components/renderCards/RenderedCards';

const HomePage = () => {
    const [results, setResults] = useState();
    const [noData, setNoData] = useState<boolean>(false);

    const api = 'https://rickandmortyapi.com/api/episode/';

    // Requests the first 20 results of the entire Rick and Morty character list.
    useEffect(() => {
        getEpisodes(api);
    }, []);

    const getEpisodes = async (endpoint: string) => {
        try {
            const response = await axios.get(endpoint);
            setResults(await response.data);
            setNoData(false);
        } catch (error) {
            console.log(error);
            setNoData(true);
        }
    };

    return (
        <section>
            <section className='home-header'>
                <div className='logos'>
                    <img
                        className='home-header--logo'
                        src={logo}
                        alt='rick and morty logo'
                    />
                    <img
                        className='home-header--character'
                        src={characteropedia}
                        alt='characteropedia'
                    />
                </div>
            </section>
            {noData ? (
                <div className='search-status'>
                    <h3 className='search-status--message'>No search results. Try again!</h3>
                    <img className='search-status--icon' src={icon} alt='rick and morty no search results' />
                </div>
            ) : (
                ''
            )}
            {results ?
            <RenderedCards
                results={results}
            /> : ''}
        </section>
    );
};

export default HomePage;