export interface Character {
    id: number;
    name: string;
    status: string;
    species: string;
    type: string;
    gender: string;
    orgin: any;
    location: any;
    image: string;
    episode: string[];
    url: string;
    created: string;
}